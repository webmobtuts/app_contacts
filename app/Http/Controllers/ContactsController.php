<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Google\Cloud\Translate\V2\TranslateClient;

class ContactsController extends Controller
{

    public function index()
    {
        $contacts = Contact::all();

        return view('home', compact('contacts'));
    }

    public function translate()
    {
        $contacts = Contact::all();

        $translate = new TranslateClient([
            'key' => env("GOOGLE_TRANSLATE_API_KEY")
        ]);

        foreach ($contacts as $key => $contact) {
            $names = json_decode($contact->names);

            $translated = [];

            foreach ($names as $name) {

                if(preg_match("/[a-zA-Z]/", $name)) {
                    $target = "ar";
                } else if(preg_match("/[أ-ي]/", $name)) {
                    $target = "en";
                }

                if($name) {
                    $result = $translate->translate($name, [
                        'target' => $target
                    ]);

                    if(isset($translated[$result['text']])) {

                        $translated[$result['text']]++;
                    } else {
                        $translated[$result['text']] = 0;
                    }
                }
            }


            // save
            $contact->modified_names = json_encode(array_keys($translated));

            $contact->modified_hits = json_encode(array_values($translated));

            $contact->save();

            return "translated";
        }
    }
}
