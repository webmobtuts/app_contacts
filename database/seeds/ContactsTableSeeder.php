<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'names' => "[\"Mohyaddin Alaoddin\",\"Edited\",\"Mohy\",\"محى الدين 2\",\"Mohy Eldiin Beinmedia\",\"Mohy Eldin\",\"Mohey Bim\",\"mohy\",\"محى\",\"دانه\",\"دانو البستكي\",\"Mohy BeInMedia\",\"null\",\"Dana Al Bastaki\",\"محى الدين\",\"فني برمجة ميديا\",\"Mohy Aldiin KW\",\"Mohyaddin\",\"+96560949399\",\"Mohy Beinmedia\",\"Mohey\",\"محيي BeInMedia\",\"Mohyaddin Saleh\",\"N أ\",\"N A\",\"Mohyaddin Saleh\",\"Mohyaddin Saleh\",\"mohy\",\"mohy\",\"mohy\",\"mohy\",\"mohy\"]",
                'hits' => json_encode($this->zeroHits(25))
            ],
            [
                'names' => "[\"لؤى\",\"Me\",\"ط.ط.أحمدالعلي٦\",\"Loe\",\"Loai Saleh\",\"لؤى saleh\",\"لؤي علاء\",\"6Al6Al A3Le\",\"ذتتي\",\"Loai Alaa Saleh\",\"لؤي\",\"لؤي اتحاد الطلاب\",\"لؤي صاحب حسن\",\"ghostfir\",\"Loai\",\"لؤي صالح\",\"Mohy Eldin\",\"Loai Esu\",\"حبيبي 4\",\"موجهة صالح\",\"loai2020@hotmail.com\"]",
                'hits' => json_encode($this->zeroHits(21))
            ],
            [
                'names' => "[\"Eimran A. f. c\",\"Imran\",\"Mohamed Imran\",\"Imran...\",\"null\",\"Imran Kdp\",\"Imran Shaik B Media\",\"محمد عمران تصليح ايفون\",\"Mohammed Imran\",\"Imran Be In Media Company\",\"Imran shaik\",\"shaik mohammad\",\"My Kuwait\",\"imran frind\",\"Imran Kwt\",\"عمران\",\"Imran Afc Kaldon\",\"Imran Fixer Client\",\"Imran Beinmedia\",\"محمد عمران\",\"Imran  Mohammed\",\"Imran. ( Shareef )\",\"عمران\",\"محمد\",\"Imran Shaik\",\"Imran Fixer\",\"Imran Mohammed\",\"Imran\",\"Imran BeInMedia\",\"محمد عمران\",\"عمران محمد\",\"Imran Beinmedia KW\",\"عمران بى ان ميديا\",\"Huh Jik\",\"محمد عمران اقامه\",\"عمران محمد خربوطلي\",\"+96566802750\",\"Imran Be In Media Ios\",\"iOS Developer\",\"إيوس مطور\",\"BeInMedia\",\"وسائل الإعلام المباشرة\",\"mohammad.imran.shaik@hotmail.com\",\"عمران شيخ\",\"أول البرمجيات مهندس\",\"Senior Software Engineer\",\"direct Media\",\"Mohamed Imran\",\"Mohamed Imran\",\"Mohamed Imran\",\"Mohamed Imran\",\"Mohamed Imran\"]",
                'hits' => json_encode($this->zeroHits(48))
            ],
            [
                'names' => "[\"MohyEldin\",\"Mohie Eldin\",\"محى الدين\",\"Mohy\",\"Mohey Bim\",\"Mohy BeInMedia\",\"Mohyaddin Alaoddin\",\"Mohy Eldin\"]",
                'hits' => json_encode($this->zeroHits(8))
            ],
            [
                'names' => "[\"صاحب الفودري\",\"صاحب الفودري (سكرتير الوزير)\",null,\"Mohamed Kamal Ahmed\",\"محمد كمال\",\"Ibrahim Kw\",\"Waked\",\"Me In Kuwait\",\"Mahmoud Waked\",\"محمود واكد Beinmedia\",\"محمود  وسائل الإعلام المباشرة\",\"Mahmud\"]",
                'hits' => json_encode($this->zeroHits(12))
            ]
        ];

        foreach ($data as $name_hits) {
            $contact = new \App\Contact();

            $contact->names = $name_hits['names'];

            $contact->hits = $name_hits['hits'];

            $contact->save();
        }
    }


    /**
     * return the hits array as zeros
     *
     * @param $num
     * @return array
     */
    public function zeroHits($num)
    {
        $hits = [];

        foreach(range(1, $num) as $value) {
            $hits[] = 0;
        }

        return $hits;
    }
}
